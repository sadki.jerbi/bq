/**
 * 
 */
package com.model.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Entity.Client;


public interface ClientRepository extends JpaRepository<Client, Long> {

}
