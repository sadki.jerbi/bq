package com.model.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Entity.Compte;

public interface CompteRepository extends JpaRepository<Compte, String> {

}
